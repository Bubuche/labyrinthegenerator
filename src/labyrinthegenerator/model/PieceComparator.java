/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.model;

import java.util.Comparator;

/**
 *
 * @author Bubuche
 */
public class PieceComparator<X extends Piece> implements Comparator<X>
{
  public PieceComparator()
  {
    
  }

  @Override
  public int compare(X o1, X o2)
  {
    if ( o1 == o2 )
      return 0;
    
    if ( o1 == null )
      return -1;
    
    if ( o2 == null )
      return 1;
    
    return o1.getId().compareTo(o2.getId());
  }
  
  
}
