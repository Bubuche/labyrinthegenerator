/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.model;

import java.util.Collection;
import java.util.Set;

/**
 * A labyrinthe represent the collection of room and store the relation between them.
 * The labyrinthe itself isn't "smart", it's a just a set of closed rooms and allow 
 * other class to "draw" a path into it.  
 * @author Bubuche
 */
public interface Labyrinthe<Y extends Comparable<Y>, X extends Piece<Y>>
{
  /**
   * Give all the room of the labyrinthe.
   * @return all the room of the labyrinthe.
   */
  public Collection<X> getPieces();
  
  /**
   * Give all the room that could be reached by the room "piece". By "could be reached" 
   * we mean : in theory, not in the actual labyrinthe. This method is used by the labyrinthe 
   * generator to know, for a given room R, which room that is adjacent to R can be oppened. 
   * @param piece The questionned room
   * @return rooms adjacent to piece.
   */
  public Set<X> getReachables(X piece);
  
  /**
   * Give a room by its id. 
   * @param c The id of the room.
   * @return The room with the id "c".
   */
  public X get(Y c);
  
  /**
   * The entrance of the labyrinthe.
   * @return The entrance of the labyrinthe.
   */
  public X getStart();
  
  /**
   * The exit of the labyrinthe.
   * @return The exit of the labyrinthe.
   */
  public X getExit();
  
  /**
   * The cost between two room. This is only used when an algorithm try to find the
   * shortest/longest path between two room. 
   * @param from 
   * @param to
   * @return The cost to go from "from" to "to".
   */
  public Float getCost(X from, X to);
}
