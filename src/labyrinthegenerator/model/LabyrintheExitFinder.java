/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.model;

/**
 *
 * @author Bubuche
 */
public interface LabyrintheExitFinder<Y extends Comparable<Y>, X extends Piece<Y>>
{
  public X getNextToExit(X current);
}
