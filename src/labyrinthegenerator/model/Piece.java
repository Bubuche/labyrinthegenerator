/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.model;

/**
 *
 * @author Bubuche
 */
public interface Piece<X extends Comparable<X>>
{
  public X getId();
}
