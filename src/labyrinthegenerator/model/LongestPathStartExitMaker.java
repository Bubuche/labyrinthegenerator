/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.model;

/**
 *
 * @author Bubuche
 */
public interface LongestPathStartExitMaker
<
  X extends Comparable<X>, 
  Y extends Piece<X>, 
  Z extends Labyrinthe<X, Y>
>
{
  public void doIt(Z labyrinthe);
}
