/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.model.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import labyrinthegenerator.model.Labyrinthe;
import labyrinthegenerator.model.Piece;
import labyrinthegenerator.tools.RandomSeedExtractor;

/**
 * Draw the labyrinthe. This class is generic and most of time you'll only need 
 * to implement some methods to give informations on the topology of the space. 
 * This class use this topology to build a spanning tree on the graph of all possible
 * ways between rooms. 
 * In most methods, the id of the room is used instead of the room itself. So, 
 * to have simpler descriptions, the word "room" is equivalent to "id of the room", even
 * if it's not perfectly true.
 * @author Bubuche
 */
public abstract class AbstractLabyrintheGenerator<
        A extends Comparable<A>, 
        B extends Piece<A>, 
        C extends Labyrinthe<A, B>,
        D
>
{

  private long randomSeed;
  protected C labyrinthe;
  
  /**
   * Build an AbstractLabyrintheGenerator with the seed of the random object. 
   * The random object itself is not modified, i.e. if you use the same random object 
   * for two different AbstractLabyrintheGenerator you'll get twice the same labyrinthe.
   * @param random The random object where the seed will be extracted.
   */
  public AbstractLabyrintheGenerator(Random random)
  {
    this.randomSeed = RandomSeedExtractor.getCurrentSeed(random);
  }

  /**
   * Build an AbstractLabyrintheGenerator with the random seed "randomSeed".
   * @param randomSeed The random seed used to make choice while drawing the labyrinth.
   */
  public AbstractLabyrintheGenerator(long randomSeed)
  {
    this.randomSeed = randomSeed;
  }

  /**
   * Draw the labyrinthe. It creates a regular labyrinthe i.e. a labyrinth with only one path
   * to the exit and no looping path. If you want to add this, you can just add some "door" randomly.
   * Be carefull, this method doesn't modify the random value in this object. If you call it twice,
   * you'll get the same result twice. 
   * 
   * For non-french readers of the code : 
   * vus = viewed, already seen.
   * piece = room.
   * atteignables = reachables
   * @return The labyrinth generated.
   */
  public C generate()
  {
    C l = labyrinthe;

    Random random = new Random(randomSeed);

    List<A> vus = new ArrayList<>();

    //initialize the generation with a room
    /**
     * /!\IMPORTANT/!\ : don't take the first room of the set by a simple "iterator().next()".
     * If you do that, as the collection is a hashset (unordered set) you'll get a random result
     * that will vary without any control.
     * Here, we take the real smallest element of the group, by comparing them with their id.
     */
    /**
     * After a modification of "getPieces()" to use the values of a TreeMap instead of the keys of 
     * a HashMap, it seems that "iterator().next()" return the same result all the time. 
     * So, we can use "B start = l.getPieces().iterator().next();" as is, once again. 
     * However, as it's not write on the javadoc that the values of a TreeMap are sorted, this
     * can change in the future. If it happens, use the commented code to find the real smallest
     * value of the set. 
     */
    Iterator<B> it;
    it = l.getPieces().iterator();
    
    B start = l.getPieces().iterator().next();
    
    /**
     * The commented code below is the fix when "B start = l.getPieces().iterator().next();"
     * return differents values over executions.
     */
    
    /*while(it.hasNext())
    {
      B other;
      other = it.next();
      
      if ( start.getId().compareTo(other.getId()) > 0 )
      {
        start = other;
      }
    }*/
    vus.add(start.getId());


    while (!vus.isEmpty())
    {
      A aPiece = selectPiece(vus, random);

      //if aPoint can open an other room, we keep it and add this other room to vus. 
      List<D> randomDirections = getAllDirectionForPiece(aPiece, random);
      boolean used = false;
      for (D direction : randomDirections)
      {
        A reader = get(aPiece, direction);
        
        if ( reader == null )
          continue;


        Set<B> atteignables = l.getReachables(l.get(reader));
        if (!atteignables.isEmpty())
        {
          continue;
        }


        l.getReachables(l.get(reader)).add(l.get(aPiece));
        l.getReachables(l.get(aPiece)).add(l.get(reader));


        vus.add(cloneId(reader));

        used = true;
        break;
      }

      /* if aPoint can't open a new room, we remove it from the vus */
      if (used == false)
      {
        vus.remove(aPiece);
      }
    }

    

    return l;
  }
  
  protected long getRandomSeed()
  {
    return this.randomSeed;
  }
  
  /**
   * Select a room in a list of possible rooms. The default implementation return
   * a room at random. You can change this comportement to affect the structure of
   * the labyrinthe. For exemple, if you return all the time the first element you'll
   * likely get a pretty geometric and artistic labyrinthe (even if very easy).
   * "pieces" cannot be empty. 
   * @param pieces A selection of possible rooms. 
   * @param random A random object to help your choice. 
   * @return The selected room.
   */
  protected A selectPiece(List<A> pieces, Random random)
  {
    return pieces.get(random.nextInt(pieces.size()));
  }

  /**
   * Return the reached room if we are in A and go in the D direction. If there is
   * no room in this direction from A, then null is returned.
   * The returned object can be reused each "get", i.e. you don't need to create a new object each call. 
   * However, if you modify it, then the "cloneId" method should really clone it, i.e. as your object is
   * mutable you have to really clone it.
   * @param from The room.
   * @param direction The direction.
   * @return The room reached if we go in the D direction from the room A, or null if there is no such room.
   */
  protected abstract A get(A from, D direction);
  
  /**
   * Return all possible directions for a room (including closed directions !). You can return too much directions
   * as long as selectPiece return null for these "wrong" directions.
   * These directions will be checked as they are ordered. It's very important not to return the same List for every room.
   * You can either shuffle it each time. You can also, if the topology of the labyrinthe is regular enough
   * (like in standard "grided" labyrinth), pre-create shuffled lists with the tool "PermutationsGenerator". 
   * @param aPiece A room.
   * @param random A random value to salt the result. 
   * @return A randomly ordered list of all possible directions.
   */
  protected abstract List<D> getAllDirectionForPiece(A aPiece, Random random);
  
  /**
   * Return a clone of the id "a" (if the id is immutable, you can return "a" itself).
   * @param a The id to clone.
   * @return The clone of the id.
   */
  protected abstract A cloneId(A a);
}
