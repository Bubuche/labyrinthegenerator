/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.model.impl;

import labyrinthegenerator.model.Piece;

/**
 *
 * @author Bubuche
 */
public class PieceImpl<X extends Comparable<X>> implements Piece<X>
{
  protected X id;
  
  public PieceImpl(X id)
  {
    this.id = id;
  }

  @Override
  public X getId()
  {
    return this.id;
  }

  
}
