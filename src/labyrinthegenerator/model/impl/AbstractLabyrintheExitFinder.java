/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.model.impl;

import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeMap;
import labyrinthegenerator.model.Labyrinthe;
import labyrinthegenerator.model.LabyrintheExitFinder;
import labyrinthegenerator.model.Piece;

/**
 *
 * @author Bubuche
 */
public abstract class AbstractLabyrintheExitFinder<
        Y extends Comparable<Y>, 
        X extends Piece<Y>,
        Z extends Labyrinthe<Y, X>
        > 
        implements LabyrintheExitFinder<Y, X>
{
  
  private Map<Y, X> directions;
  
  public AbstractLabyrintheExitFinder()
  {
    directions = new TreeMap<>();
  }
  
  public void init(Z labyrinthe)
  {
    directions.clear();
    PriorityQueue<Reached> reached = new PriorityQueue<>();
    
    reached.add(new Reached(0, labyrinthe.getExit()));
    directions.put(labyrinthe.getExit().getId(), labyrinthe.getExit());
    
    while( ! reached.isEmpty() )
    {
      Reached first = reached.poll();
      float distance = first.distance;
      
      Set<X> atteignables = labyrinthe.getReachables(first.piece);
      
      for ( X voisin : atteignables )
      {
        if ( directions.containsKey(voisin.getId()) )
        {
          continue;
        }
        float d = distance + labyrinthe.getCost(first.piece, voisin);
        
        directions.put(voisin.getId(), first.piece);
        reached.add(new Reached(d, voisin));
      }
    }
  }

  @Override
  public X getNextToExit(X current)
  {
    return directions.get(current.getId());
  }
  
  private class Reached implements Comparable<Reached>
  {
    public float distance;
    public X piece;

    public Reached(float distance, X piece)
    {
      this.distance = distance;
      this.piece = piece;
    }

    @Override
    public int compareTo(Reached o)
    {
      if ( o == this )
      {
        return 0;
      }
      if ( o == null )
      {
        return 1;
      }
      
      int v;
      v = Float.compare(distance, o.distance);
      if ( v != 0 )
      {
        return v;
      }
      
      v = piece.getId().compareTo(o.piece.getId());
      
      return v;
    }
    
    
  }
  
}
