/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.model.impl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import labyrinthegenerator.model.Labyrinthe;
import labyrinthegenerator.model.Piece;
import labyrinthegenerator.model.PieceComparator;

/**
 *
 * @author Bubuche
 */
public class LabyrintheImpl<Y extends Comparable<Y>, X extends Piece<Y>> implements Labyrinthe<Y, X>
{
  private Map<X, Set<X>> atteignables;
  private Map<Y, X> pieces;
  private Collection<X> const_pieces;
  private X depart;
  private X arrive;
  
  private PieceComparator<X> pieceComparator;
  
  
  public LabyrintheImpl()
  {
    pieceComparator = new PieceComparator<>();
    atteignables = new HashMap<>(); //new TreeMap<>(pieceComparator);
    pieces = new TreeMap<>();
    const_pieces = Collections.unmodifiableCollection(pieces.values());
  }

  @Override
  public Collection<X> getPieces()
  {
    return const_pieces;
  }

  @Override
  public Set<X> getReachables(X piece)
  {
    return atteignables.get(piece);
  }

  @Override
  public X getStart()
  {
    return this.depart;
  }

  @Override
  public X getExit()
  {
    return this.arrive;
  }
  
  public void setStart(X depart)
  {
    this.depart = depart;
  }
  
  public void setExit(X arrive)
  {
    this.arrive = arrive;
  }
  
  public void addPiece(X piece)
  {
    if ( atteignables.containsKey(piece) )
      return;
    
    atteignables.put(piece, new TreeSet<>(pieceComparator));
    pieces.put(piece.getId(), piece);
  }

  @Override
  public X get(Y c)
  {
    return pieces.get(c);
  }
  
  @Override
  public Float getCost(X from, X to)
  {
    if ( atteignables.get(from).contains(to) )
    {
      return Float.valueOf(1);
    }
    else
    {
      return Float.POSITIVE_INFINITY;
    }
  }
}
