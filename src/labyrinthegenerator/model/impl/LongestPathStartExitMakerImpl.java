/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.model.impl;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import labyrinthegenerator.model.LongestPathStartExitMaker;
import labyrinthegenerator.model.Piece;

public class LongestPathStartExitMakerImpl<
  X extends Comparable<X>
, Y extends Piece<X>
, Z extends LabyrintheImpl<X, Y>
> implements LongestPathStartExitMaker<X, Y, Z>
{
  
  private List<FarRoom<Y>> preparedRooms = new LinkedList<>();

  @Override
  public void doIt(Z labyrinthe)
  {
    Y init;
    init = labyrinthe.getPieces().iterator().next();
    
    Y start = findFartherIter(labyrinthe, init);
    Y exit = findFartherIter(labyrinthe, start);
    
    labyrinthe.setStart(start);
    labyrinthe.setExit(exit);
    
  }
  
  private FarRoom<Y> createFarRoom()
  {
    if ( ! preparedRooms.isEmpty() )
    {
      return preparedRooms.remove(0);
    }
    else
    {
      return new FarRoom<>();
    }
  }
  
  private void releaseFarRoom(FarRoom<Y> room)
  {
    preparedRooms.add(room);
  }
  
  private Y findFartherIter(Z labyrinthe, Y room)
  {
    List<FarRoom<Y>> viewed;
    viewed = new LinkedList<>();
    
    List<FarRoom<Y>> limit;
    limit = new LinkedList<>();
    
    
    FarRoom<Y> max = createFarRoom();
    max.cost = 0f;
    max.room = room;
    max.parent = null;
    viewed.add(max);
    
    while(!viewed.isEmpty())
    {
      FarRoom<Y> r = viewed.remove(0);

      Set<Y> reachables = labyrinthe.getReachables(r.room);
      if ( reachables.size() == 1 && r.parent != null )
      {
        limit.add(r);
        continue;
      }

      for ( Y reachable : reachables )
      {
        if ( reachable == r.parent )
        {
          continue;
        }
        FarRoom<Y> n = new FarRoom();
        n.cost = r.cost + labyrinthe.getCost(r.room, reachable);
        n.room = reachable;
        n.parent = r.room;

        viewed.add(n);
      }
      releaseFarRoom(r);
    }
    
    max = null;
    for ( FarRoom<Y> l : limit )
    {
      if ( max == null || l.cost > max.cost )
      {
        max = l;
      }
      
    }
    Y result = max.room;
    for ( FarRoom<Y> l : limit )
    {
      releaseFarRoom(l);
    }
    
    return result;
  }
  
  private FarRoom<Y> findFarther(Z labyrinthe, Y room, Y parent)
  {
    Set<Y> reachables = labyrinthe.getReachables(room);
    if ( reachables.size() == 1 && parent != null )
    {
      FarRoom<Y> result = new FarRoom();
      result.cost = labyrinthe.getCost(parent, room);
      result.room = room;
      
      return result;
    }
    
    FarRoom<Y> result = null;
    for ( Y y : reachables )
    {
      if ( y == parent )
      {
        continue;
      }
      
      FarRoom<Y> child;
      child = findFarther(labyrinthe, y, room);
      Float cost = labyrinthe.getCost(room, y) + child.cost;
      if ( result == null || result.cost < cost )
      {
        result = child;
        result.cost = cost;
      }
    }
    
    return result;
  }
  
  private void recurseFindPaths(
          Z labyrinthe
        , Y room
        , TreeMap<Float, List<Y>> n 
        , TreeMap<Float, List<Y>> paths
        , Y parent)
  {
    n.clear();
    
    Set<Y> reachables = labyrinthe.getReachables(room);
    for ( Y y : reachables )
    {
      if ( y == parent )
      {
        continue;
      }
      
      recurseFindPaths(labyrinthe, y, n, paths, room);
    }
    
  }
  
  private void test1(Z labyrinthe)
  {
    Map<X, FoldingRoom> rooms;
    rooms = new TreeMap<>();

    Set<X> done = new TreeSet<>();


    /* initialisation : all "dead end" */
    for (Y piece : labyrinthe.getPieces())
    {
      if (labyrinthe.getReachables(piece).size() != 1)
      {
        continue;
      }

      FoldingRoom fr = new FoldingRoom();
      fr.id = piece.getId();
      fr.cost = 0f;
      fr.prec = null;
      fr.nexts = labyrinthe.getReachables(piece);
      rooms.put(fr.id, fr);
    }

    System.out.println(rooms.size());

    FoldingRoom toFold;

    while (rooms.size() > 2)
    {
      toFold = null;
      Iterator<Map.Entry<X, FoldingRoom>> it;
      it = rooms.entrySet().iterator();

      while (it.hasNext())
      {
        Map.Entry<X, FoldingRoom> entry = it.next();

        Set<Y> nexts = entry.getValue().nexts;
        int notFolded = 0;

        for (Y next : nexts)
        {
          X id = next.getId();
          if (done.contains(id))
          {
            continue;
          }
          if (!rooms.containsKey(next.getId()))
          {
            notFolded += 1;
            if (notFolded > 1)
            {
              break;
            }
          }
        }

        System.out.println(notFolded);
        if (notFolded > 1)
        {
          continue;
        }
        if ( notFolded == 0 )
        {
          break;
        }
        toFold = entry.getValue();
        break;
      }


      if ( toFold == null )
      {
        break;
      }
      
      rooms.remove(toFold.id);

      FoldingRoom maxCosts = null;
      Y notFolded = null;

      for (Y next : toFold.nexts)
      {
        X id = next.getId();
        if (done.contains(id))
        {
          continue;
        }
        if (!rooms.containsKey(next.getId()))
        {
          notFolded = next;
          continue;
        }
        FoldingRoom other = rooms.get(next.getId());
        if (maxCosts == null || other.cost > maxCosts.cost)
        {
          maxCosts = other;
        }
      }

      for (Y piece : toFold.nexts)
      {
        rooms.remove(piece.getId());
      }

      if (maxCosts != null)
      {
        toFold.cost += maxCosts.cost;
      }


      toFold.cost += 1;

      if (notFolded == null)
      {
        System.out.println(rooms.keySet());
      }
      toFold.id = notFolded.getId();
      toFold.nexts = labyrinthe.getReachables(notFolded);
      toFold.prec = maxCosts;
      rooms.put(toFold.id, toFold);

    }

    assert (rooms.size() == 2);

    Iterator<FoldingRoom> it = rooms.values().iterator();
    FoldingRoom forStart = it.next();
    FoldingRoom forExit = it.next();

    while (forStart.prec != null)
    {
      forStart = forStart.prec;
    }

    while (forExit.prec != null)
    {
      forExit = forExit.prec;
    }

    labyrinthe.setStart(labyrinthe.get(forStart.id));
    labyrinthe.setExit(labyrinthe.get(forExit.id));


  }

  private class FoldingRoom
  {

    private X id;
    private Float cost;
    private FoldingRoom prec;
    private Set<Y> nexts;
  }

  private static class FarRoom<Y>
  {
    public Y room;
    public Y parent;
    public float cost;
  }
}
