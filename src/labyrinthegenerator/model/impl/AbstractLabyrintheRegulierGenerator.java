/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.model.impl;

import java.util.List;
import java.util.Random;
import labyrinthegenerator.model.Labyrinthe;
import labyrinthegenerator.model.Piece;
import labyrinthegenerator.tools.PermutationsGenerator;

/**
 * Provide an abstract level to simplify the creation of labyrinth with a regular topology. 
 * @author Bubuche
 */
public abstract class AbstractLabyrintheRegulierGenerator<
        A extends Comparable<A>, 
        B extends Piece<A>, 
        C extends Labyrinthe<A, B>,
        D
    >
      extends AbstractLabyrintheGenerator<A, B, C, D>
{
  private List<List<D>> directions_permutations;

  public AbstractLabyrintheRegulierGenerator(Random random)
  {
    super(random);
    directions_permutations = createDirections(getAllDirections());
  }

  public AbstractLabyrintheRegulierGenerator(long randomSeed)
  {
    super(randomSeed);
    directions_permutations = createDirections(getAllDirections());
  }
  
  private List<List<D>> createDirections(D[] directions)
  {
    return PermutationsGenerator.generate(directions);
  }
  
  @Override
  protected List<D> getAllDirectionForPiece(A aPiece, Random random)
  {
    return directions_permutations.get(random.nextInt(directions_permutations.size()));
  }
  
  /**
   * Return all directions possibly used in the labyrinth. This should be a small array. 
   * If you have many possible directions in your labyrinth, you should use AbstractLabyrintheGenerator instead
   * of AbstractLabyrintheRegulierGenerator.
   * @return All directions possibly used in the labyrinth.
   */
  protected abstract D[] getAllDirections();
  
}
