/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.model.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import labyrinthegenerator.model.LongestPathStartExitMaker;
import labyrinthegenerator.model.Piece;

/**
 * This class do the same job as 
 * @author Bubuche
 * @param <X>
 * @param <Y>
 * @param <Z> 
 */
public class LPSEMWithLoop<
  X extends Comparable<X>
, Y extends Piece<X>
, Z extends LabyrintheImpl<X, Y>
> implements LongestPathStartExitMaker<X, Y, Z>
{
  
  private List<FarRoom<Y>> preparedRooms = new LinkedList<>();

  @Override
  public void doIt(Z labyrinthe)
  {
    Y init;
    init = labyrinthe.getPieces().iterator().next();
    
    Y start = findFartherIter(labyrinthe, init);
    Y exit = findFartherIter(labyrinthe, start);
    
    labyrinthe.setStart(start);
    labyrinthe.setExit(exit);
    
  }
  
  private FarRoom<Y> createFarRoom()
  {
    FarRoom<Y> result;
    if ( ! preparedRooms.isEmpty() )
    {
       result = preparedRooms.remove(0);
       result.parent = null;
    }
    else
    {
      result = new FarRoom<>();
    }
    return result;
  }
  
  private void releaseFarRoom(FarRoom<Y> room)
  {
    preparedRooms.add(room);
  }
  
  private Y findFartherIter(Z labyrinthe, Y room)
  {
    List<FarRoom<Y>> viewed;
    viewed = new LinkedList<>();
    
    List<FarRoom<Y>> limit;
    limit = new LinkedList<>();
    
    
    FarRoom<Y> max = createFarRoom();
    max.cost = 0f;
    max.room = room;
    max.parent = null;
    max.directParent = null;
    viewed.add(max);
    
    while(!viewed.isEmpty())
    {
      FarRoom<Y> r = viewed.remove(0);

      Set<Y> reachables = labyrinthe.getReachables(r.room);
      if ( reachables.size() == 1 && r.directParent != null )
      {
        limit.add(r);
        continue;
      }

      LinkedListWithDifferentEnds<Y> parent;
      if ( reachables.size() < 3 )
      {
        parent = r.parent;
      }
      else
      {
        parent = new LinkedListWithDifferentEnds<>(r.parent, r.room);
      }
        
      for ( Y reachable : reachables )
      {
        if ( r.parent != null && r.parent.contains(reachable) )
        {
          continue;
        }
        if ( r.directParent == reachable )
        {
          continue;
        }
        
        FarRoom<Y> n = createFarRoom();
        n.cost = r.cost + labyrinthe.getCost(r.room, reachable);
        n.room = reachable;
        n.parent = parent;
        n.directParent = r.room;

        viewed.add(n);
      }
      releaseFarRoom(r);
    }
    
    max = null;
    for ( FarRoom<Y> l : limit )
    {
      if ( max == null || l.cost > max.cost )
      {
        max = l;
      }
      
    }
    Y result = max.room;
    for ( FarRoom<Y> l : limit )
    {
      releaseFarRoom(l);
    }
    
    return result;
  }

  private static class FarRoom<Y>
  {    
    public Y room;
    public LinkedListWithDifferentEnds<Y> parent;
    public Y directParent;
    public float cost;
  }
  
  private static class LinkedListWithDifferentEnds<X>
  {
    private LinkedListWithDifferentEnds<X> parent;
    private X value;
    
    public LinkedListWithDifferentEnds(LinkedListWithDifferentEnds<X> parent, X value)
    {
      this.parent = parent;
      this.value = value;
    }
    
    public LinkedListWithDifferentEnds(X value)
    {
      this.value = value;
    }
    
    public boolean contains(X value)
    {
      /* p is used to avoid recursivity */
      LinkedListWithDifferentEnds<X> p;
      p = this;
      
      while(p != null)
      {
        if ( p.value == value )
        {
          return true;
        }
        if ( p.parent == null )
        {
          return false;
        }
        p = p.parent;
      }
      return false;
    }
  }
}
