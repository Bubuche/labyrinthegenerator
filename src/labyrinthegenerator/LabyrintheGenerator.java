/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import labyrinthegenerator.tools.LabyrintheTriangleToImage;

/**
 *
 * @author Bubuche
 */
public class LabyrintheGenerator
{

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args)
  {
    try
    {
      LabyrintheTriangleToImage.main(args);
    }
    catch (IOException ex)
    {
      Logger.getLogger(LabyrintheGenerator.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}
