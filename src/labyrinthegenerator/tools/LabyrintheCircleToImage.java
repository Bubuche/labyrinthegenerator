/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.tools;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import labyrinthegenerator.examples.circle.LabyrintheCircle;
import labyrinthegenerator.examples.circle.LabyrintheCircleGenerator;
import labyrinthegenerator.examples.circle.PieceCircle;
import labyrinthegenerator.examples.circle.PieceCircleId;
import labyrinthegenerator.model.impl.LongestPathStartExitMakerImpl;

/**
 *
 * @author Bubuche
 */
public class LabyrintheCircleToImage
{
  public static BufferedImage create(LabyrintheCircle laby, int scale)
  {
    int w, h;
    int scale_2 = scale/2;
    
    w = laby.levels() * scale;
    h = w;
    
    
    BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
    int center_x, center_y;
    center_x = w/2;
    center_y = h/2;
    
    Graphics2D g2d = image.createGraphics();
    g2d.setColor(Color.white);
    g2d.fillRect(0, 0, w, h);
    
    
    g2d.setColor(Color.black);
    
    for ( int i = 0; i < laby.levels(); i++ )
    {
      int size = (i+1) * scale;
      int size_2 = size / 2;
      g2d.drawOval(center_x - size_2, center_y - size_2, size, size);
    }
    
    

    LabyrintheCircleGenerator.MutableCircleId testPiece = new LabyrintheCircleGenerator.MutableCircleId();
    
    Stroke normal, cleaner;
    
    normal = g2d.getStroke();
    cleaner = new BasicStroke(3);
    Color door = Color.gray;
    Color wall = Color.black;
    
    float angle1, angle2;
    for ( PieceCircle piece : laby.getPieces() )
    {
      PieceCircleId id = piece.getId();
      int level = id.getLevel();
      float size = sizeOf(id);
      angle1 = (id.getAngle()) * size;
      angle2 = (id.getAngle()+1) * size;
      //System.out.println(id.getLevel()+":"+id.getAngle()+" "+size/Math.PI);
      
      g2d.setColor(wall);
      g2d.setStroke(normal);
      
      //System.out.println(piece.getId()+" "+laby.getReachables(piece));
      
      
      
      
      int x1, y1, x2, y2;
      
      float cos, sin;
      int other_angle;
      testPiece.setLevel(level);
      
      other_angle = id.getAngle() - 1;
      if ( other_angle < 0 )
      {
        other_angle = level;
      }
      testPiece.setAngle(other_angle);
      if ( laby.getReachables(piece).contains(laby.get(testPiece)) )
      {
        g2d.setColor(door);
        g2d.setStroke(cleaner);
      }
      else
      {
        g2d.setColor(wall);
        g2d.setStroke(normal);
      }
      
      cos = (float) Math.cos(angle1);
      sin = -(float) Math.sin(angle1);

      x1 = (int) (cos * scale_2 * level);
      y1 = (int) (sin * scale_2 * level);

      x2 = (int) (cos * scale_2 * (level+1));
      y2 = (int) (sin * scale_2 * (level+1));

      g2d.drawLine(x1 + center_x, y1 + center_y, x2 + center_x, y2 + center_y);
      //System.out.println((x1 + center_x)+", "+(y1 + center_y)+", "+(x2 + center_x)+", "+(y2 + center_y));
      
      other_angle = id.getAngle() + 1;
      if ( other_angle > level )
      {
        other_angle = 0;
      }
      testPiece.setAngle(other_angle);
      if ( laby.getReachables(piece).contains(laby.get(testPiece)) )
      {
        g2d.setColor(door);
        g2d.setStroke(cleaner);
      }
      else
      {
        g2d.setColor(wall);
        g2d.setStroke(normal);
      }
      cos = (float) Math.cos(angle2);
      sin = -(float) Math.sin(angle2);

      x1 = (int) (cos * scale_2 * level);
      y1 = (int) (sin * scale_2 * level);

      x2 = (int) (cos * scale_2 * (level+1));
      y2 = (int) (sin * scale_2 * (level+1));

      g2d.drawLine(x1 + center_x, y1 + center_y, x2 + center_x, y2 + center_y);
      //System.out.println((x1 + center_x)+", "+(y1 + center_y)+", "+(x2 + center_x)+", "+(y2 + center_y));
      
      g2d.setColor(door);
        
      g2d.setStroke(cleaner);
      for ( PieceCircle reachable : laby.getReachables(piece) )
      {
        float testAngle;
        
        PieceCircleId other_id = reachable.getId();
        if ( other_id.getLevel() == level )
        {
          continue;
        }
        
        float other_size = sizeOf(other_id);
        angle1 = size * id.getAngle();
        testAngle = other_size * (other_id.getAngle());
        
        if ( angle1 < testAngle )
        {
          angle1 = testAngle;
        }
        
        angle2 = (size) * (id.getAngle()+1);
        testAngle = (other_size) * ((other_id.getAngle()+1));
        
        if ( angle2 > testAngle )
        {
          angle2 = testAngle;
        }
        
        if ( angle2 < angle1 )
        {
          testAngle = angle1;
          angle1 = angle2;
          angle2 = testAngle;
        }
        
        int d1, d2;
        d1 = degOf(angle1);
        d2 = degOf(angle2);
        d2 -= d1;
        //System.out.println(id+" to "+other_id+" = "+d1+" to "+d2+" "+w);
        
        int dif;
        if ( other_id.getLevel() > level )
        {
          dif = 1;
        }
        else
        {
          dif = 0;
        }
        
        int x, y;
        w = scale * (level+dif);
        h = w;
        
        x = center_x - (w/2);
        y = x;
      
        g2d.drawArc(x, y, w, h, d1, d2);
      }
    }
    
    
    
    g2d.dispose();
    
    return image;
  }
  
  private static int degOf(float rad)
  {
    return (int) ((rad * 180) / Math.PI);
  }
  
  private static float sizeOf(PieceCircleId id)
  {
    return (float) ((Math.PI * 2) / (id.getLevel()+1));
  }
  
  public static void main(String args[]) throws IOException
  {
    test();
  }
  
  //public static Map<PieceCircleId, Integer> numbers = new TreeMap<>();
  
  public static void test() throws IOException
  {
    
    long seed = 3;
    int scale = 32;
    LabyrintheCircleGenerator generator = new LabyrintheCircleGenerator(seed);
    generator.setSize(5);

    LabyrintheCircle laby = generator.generate();
    
    LongestPathStartExitMakerImpl<PieceCircleId, PieceCircle, LabyrintheCircle> longest;
    longest = new LongestPathStartExitMakerImpl<>();
    
    longest.doIt(laby);
    
    
    
    BufferedImage bi = create(laby, scale);
    //addPath(bi, laby, scale);


    ImageIO.write(bi, "png", new File("test.png"));
  }
}
