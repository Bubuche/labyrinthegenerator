/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.tools;

import java.util.TreeSet;
import labyrinthegenerator.examples.l2d.Point2i;

/**
 *
 * @author Bubuche
 */
public class Point2iGenerator
{
  public static final Point2iGenerator instance = new Point2iGenerator();

  private TreeSet<Point2i> points;
  private MutablePoint2i lookUp;

  public Point2iGenerator()
  {
    points = new TreeSet<>();
    lookUp = new MutablePoint2i();
  }

  public Point2i get(int x, int y)
  {
    lookUp.set(x, y);
    if ( ! points.contains(lookUp) )
    {
      Point2i result = new Point2i(x, y);
      points.add(result);
      return result;
    }
    else
    {
      return points.floor(lookUp);
    }
  }

  private static class MutablePoint2i extends Point2i
  {

    public MutablePoint2i()
    {
      super(0, 0);
    }
    
    public void setX(int x)
    {
      this.x = x;
    }

    public void setY(int y)
    {
      this.y = y;
    }
   
    public void set(int x, int y)
    {
      this.x = x;
      this.y = y;
    }
  }
}
