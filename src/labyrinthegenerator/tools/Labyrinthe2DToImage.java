/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.tools;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import labyrinthegenerator.examples.l2d.Labyrinthe2D;
import labyrinthegenerator.examples.l2d.Labyrinthe2DExitFinder;
import labyrinthegenerator.examples.l2d.Labyrinthe2DGenerator;
import labyrinthegenerator.examples.l2d.Piece2D;
import labyrinthegenerator.examples.l2d.Point2i;
import labyrinthegenerator.model.LongestPathStartExitMaker;
import labyrinthegenerator.model.impl.LPSEMWithLoop;
import labyrinthegenerator.model.impl.LongestPathStartExitMakerImpl;

/**
 *
 * @author Bubuche
 */
public class Labyrinthe2DToImage
{

  public static BufferedImage create(Labyrinthe2D l2d)
  {
    BufferedImage bi = new BufferedImage(l2d.getWidth() * 3, l2d.getHeight() * 3, BufferedImage.TYPE_INT_ARGB);

    Graphics g = bi.getGraphics();
    g.setColor(Color.BLACK);
    g.fillRect(0, 0, bi.getWidth(), bi.getHeight());

    int white = Color.white.getRGB();
    int arrive_color = Color.red.getRGB();
    int depart_color = Color.green.getRGB();

    int chemin_color = Color.blue.getRGB();

    int center_x, center_y;
    for (Piece2D p2d : l2d.getPieces())
    {
      Point2i id = p2d.getId();
      center_x = (id.getX() * 3) + 1;
      center_y = (id.getY() * 3) + 1;

      if (p2d == l2d.getStart())
      {
        bi.setRGB(center_x, center_y, depart_color);
      }
      else if (p2d == l2d.getExit())
      {
        bi.setRGB(center_x, center_y, arrive_color);
      }
      else
      {
        bi.setRGB(center_x, center_y, white);
      }

      for (Piece2D neighbour : l2d.getReachables(p2d))
      {
        Point2i idNeighbour = neighbour.getId();
        int x, y;
        x = idNeighbour.getX() - id.getX();
        y = idNeighbour.getY() - id.getY();

        bi.setRGB(center_x + x, center_y + y, white);
      }
    }

    Labyrinthe2DExitFinder exitFinder = new Labyrinthe2DExitFinder();
    exitFinder.init(l2d);

    Piece2D depart = l2d.getStart(); // = l2d.get(new  Point2i(1, 3));
    List<Piece2D> chemin = PathMaker.create(exitFinder, depart);

    for (Piece2D p2d : chemin)
    {
      Point2i id = p2d.getId();
      bi.setRGB(id.getX() * 3 + 1, id.getY() * 3 + 1, chemin_color);
    }

    return bi;
  }

  public static void main(String args[]) throws IOException
  {
    exampleWithLoop();
  }

  public static void exampleStandard() throws IOException
  {
    long seed = 2;

    Labyrinthe2DGenerator l2dg = new Labyrinthe2DGenerator(seed);
    l2dg.setSize(100, 100);
    Labyrinthe2D l2d = l2dg.generate();

    LongestPathStartExitMaker<Point2i, Piece2D, Labyrinthe2D> startExitMaker;

    startExitMaker = new LongestPathStartExitMakerImpl<>();
    startExitMaker.doIt(l2d);
    BufferedImage bi = create(l2d);



    ImageIO.write(bi, "png", new File("test.png"));
  }

  public static void exampleWithLoop() throws IOException
  {
    long seed = 2;

    Labyrinthe2DGenerator l2dg = new Labyrinthe2DGenerator(seed);
    l2dg.setSize(100, 100);
    Labyrinthe2D l2d = l2dg.generate();

    /**
     * Create a loop in the labyrinth. Actually, this work only for this seed
     * and this size etc. as the door we open (between (10,10) and (9,10) could
     * be already opened. If this doesn't create a loop, identifiy a couple of
     * room on the image which are not connected together and open the wall
     * between them. This will automatically lead to a loop.
     */
    Point2i f, t;
    f = Point2iGenerator.instance.get(10, 10);
    t = Point2iGenerator.instance.get(9, 10);

    l2d.getReachables(l2d.get(f)).add(l2d.get(t));
    l2d.getReachables(l2d.get(t)).add(l2d.get(f));

    LongestPathStartExitMaker<Point2i, Piece2D, Labyrinthe2D> startExitMaker;

    /**
     * If you plan to use a labyrinth with path which loop, you must don't use
     * the LongestPathStartExitMakerImpl (as it will loop forever) but the
     * LPSEMWithLoop. This one is a bit slower, however.
     */
    startExitMaker = new LPSEMWithLoop<>();
    startExitMaker.doIt(l2d);

    BufferedImage bi = create(l2d);

    ImageIO.write(bi, "png", new File("test.png"));
  }
}
