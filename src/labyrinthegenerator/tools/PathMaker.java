/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.tools;

import java.util.ArrayList;
import java.util.List;
import labyrinthegenerator.model.Labyrinthe;
import labyrinthegenerator.model.LabyrintheExitFinder;
import labyrinthegenerator.model.Piece;

/**
 *
 * @author Bubuche
 */
public class PathMaker
{
  public static <
          X extends Comparable<X>, 
          Y extends Piece<X>, 
          Z extends LabyrintheExitFinder<X, Y>
          > 
          List<Y> create(Z exitFinder, Y piece)
  {
    List<Y> l = new ArrayList<>();
    
    while(exitFinder.getNextToExit(piece) != piece )
    {
      piece = exitFinder.getNextToExit(piece);
      l.add(piece);
    }
    
    return l;
  }

}
