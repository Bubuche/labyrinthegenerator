/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.tools;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Bubuche
 */
public class PermutationsGenerator
{
  public static <X> List<List<X>> generate(X[] objects)
  {

    Object[][] build = new Object[][] { new Object[]{} };
    
    for ( Object direction :  objects )
    {
      build = add(build, direction);
    }
    
    List<List<X>> result = new ArrayList<>();
    for ( int i = 0; i < build.length; i++ )
    {
      List<X> permutations = new ArrayList<>();
      for ( int j = 0; j < build[i].length; j++ )
      {
        permutations.add((X) build[i][j]);
      }
      result.add(permutations);
    }
    
    return result;
  }
  
  public static int factoriel(int n)
  {
    int result = 1;
    while(n > 1)
    {
      result *= n;
      n--;
    }
    return result;
  }
  
  public static Object[][] add(Object[][] before, Object direction)
  {
    Object[][] result = new Object[before.length * (before[0].length + 1)][];
    int i = 0;
    
    for ( Object[] directions : before )
    {
      for ( int p = 0; p <= directions.length; p++ )
      {
        result[i] = new Object[directions.length + 1];
        for ( int k = 0; k < p; k++ )
        {
          result[i][k] = directions[k];
        }
        result[i][p] = direction;
        for ( int k = p; k < directions.length; k++ )
        {
          result[i][k+1] = directions[k];
        }
        i++;
      }
    }
    return result;
  }
}






