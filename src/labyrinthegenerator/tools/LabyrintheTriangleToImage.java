/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.tools;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.imageio.ImageIO;
import labyrinthegenerator.model.impl.LongestPathStartExitMakerImpl;
import labyrinthegenerator.examples.l2d.Point2i;
import labyrinthegenerator.examples.triangle.LabyrintheTriangle;
import labyrinthegenerator.examples.triangle.LabyrintheTriangleExitFinder;
import labyrinthegenerator.examples.triangle.LabyrintheTriangleGenerator;
import labyrinthegenerator.examples.triangle.PieceTriangle;

/**
 *
 * @author Bubuche
 */
public class LabyrintheTriangleToImage
{

  public static BufferedImage create(LabyrintheTriangle laby, int scale)
  {
    int deepness = laby.deepness();
    
    int w, h;
    w = (2 << deepness) * scale;
    h = (2 << deepness) * scale;

    BufferedImage result = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
    Graphics2D g2d = result.createGraphics();
    g2d.setColor(Color.white);
    g2d.fillRect(0, 0, w, h);
    g2d.setColor(Color.black);

    
    int scale_2 = scale / 2;
    
    MutablePoint2i p1, p2, p3;
    p1 = new MutablePoint2i();
    p2 = new MutablePoint2i();
    p3 = new MutablePoint2i();
    

    for (PieceTriangle pt : laby.getPieces())
    {
      Point2i id;
      id = pt.getId();
      int x, y;
      x = id.getX();
      y = id.getY();

      int xs, ys;
      xs = x * scale;
      ys = y * scale;

      if ( pt == laby.getStart() )
      {
        result.setRGB(xs, ys, Color.red.getRGB());
      }
      else if ( pt == laby.getExit() )
      {
        result.setRGB(xs, ys, Color.blue.getRGB());
      }
      //g2d.drawString(id.toString(), xs, ys);

      int x1, y1, x2, y2, x3, y3;
      
      Set<PieceTriangle> reachables;
      reachables = laby.getReachables(pt);

      if (pt.reversed())
      {
        x1 = xs + scale;
        y1 = ys + scale;
        p1.set(x + 1, y);

        x2 = xs;
        y2 = ys - scale;
        p2.set(x - 1, y);

        x3 = xs - scale;
        y3 = ys + scale;
        p3.set(x, y+2);
      }
      else
      {
        x1 = xs - scale;
        y1 = ys - scale;
        p1.set(x-1, y);

        x2 = xs;
        y2 = ys + scale;
        p2.set(x+1, y);

        x3 = xs + scale;
        y3 = ys - scale;
        p3.set(x, y-2);
      }
      
      if ( ! reachables.contains(laby.get(p1)) )
      {
        g2d.drawLine(x1, y1, x2, y2);
      }
      if ( ! reachables.contains(laby.get(p2)) )
      {
        g2d.drawLine(x2, y2, x3, y3);
      }
      
      if ( ! reachables.contains(laby.get(p3)) )
      {
        g2d.drawLine(x3, y3, x1, y1);
      }
    }

    g2d.dispose();
    
    return result;
  }
  
  public static void addPath(BufferedImage image, LabyrintheTriangle laby, int scale)
  {
    LabyrintheTriangleExitFinder exitFinder = new LabyrintheTriangleExitFinder();
    
    exitFinder.init(laby);
    
    List<PieceTriangle> pieces = PathMaker.create(exitFinder, laby.getStart());
    
    Graphics2D g = image.createGraphics();
    g.setColor(Color.red);
    
    PieceTriangle prec;
    prec = laby.getStart();
    boolean removeOne = true;
    for (Iterator<PieceTriangle> it = pieces.iterator(); it.hasNext();)
    {
      it.next();
      if ( removeOne )
      {
        it.remove();
      }
      removeOne = ! removeOne;
    }
    
    int scale_2 = scale/2;
    
    for ( PieceTriangle piece : pieces )
    {
      Point2i p1, p2;
      p1 = prec.getId();
      p2 = piece.getId();
      
      g.drawLine(
              p1.getX() * scale, 
              p1.getY() * scale, 
              p2.getX() * scale, 
              p2.getY() * scale);
      prec = piece;
    }
    
    g.dispose();
  }

  public static void main(String args[]) throws IOException
  {
    long seed = 3;
    int scale = 4;
    LabyrintheTriangleGenerator generator = new LabyrintheTriangleGenerator(seed);
    generator.setSize(6);

    LabyrintheTriangle laby = generator.generate();
    
    LongestPathStartExitMakerImpl<Point2i, PieceTriangle, LabyrintheTriangle> longest;
    longest = new LongestPathStartExitMakerImpl<>();
    
    longest.doIt(laby);

    BufferedImage bi = create(laby, scale);
    addPath(bi, laby, scale);


    ImageIO.write(bi, "png", new File("test.png"));
  }
  
  private static class MutablePoint2i extends Point2i
  {

    public MutablePoint2i()
    {
      super(0, 0);
    }

    public void setX(int x)
    {
      this.x = x;
    }

    public void setY(int y)
    {
      this.y = y;
    }
    
    public void set(int x, int y)
    {
      this.x = x;
      this.y = y;
    }
  }
}
