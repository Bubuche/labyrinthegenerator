/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.tools;

import java.lang.reflect.Field;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bubuche
 */
public class RandomSeedExtractor
{

  public static long getCurrentSeed(Random random)
  {
    try
    {
      // Access private field to get the seed
      Field seedField = random.getClass().getDeclaredField("seed");
      seedField.setAccessible(true);
      AtomicLong seedFieldValue = (AtomicLong) seedField.get(random);
      // Unperturb the seed from the magic multiplier
      return seedFieldValue.get() ^ 0x5DEECE66DL;
    }
    catch (IllegalArgumentException ex)
    {
      Logger.getLogger(RandomSeedExtractor.class.getName()).log(Level.SEVERE, null, ex);
    }
    catch (IllegalAccessException ex)
    {
      Logger.getLogger(RandomSeedExtractor.class.getName()).log(Level.SEVERE, null, ex);
    }
    catch (NoSuchFieldException ex)
    {
      Logger.getLogger(RandomSeedExtractor.class.getName()).log(Level.SEVERE, null, ex);
    }
    catch (SecurityException ex)
    {
      Logger.getLogger(RandomSeedExtractor.class.getName()).log(Level.SEVERE, null, ex);
    }
    return 0;
  }
}
