/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.triangle;

import labyrinthegenerator.model.impl.PieceImpl;
import labyrinthegenerator.examples.l2d.Point2i;

/**
 *
 * @author Bubuche
 */
public class PieceTriangle extends PieceImpl<Point2i>
{
  private boolean reverse;
  
  public PieceTriangle(Point2i id, boolean reverse)
  {
    super(id);
    this.reverse = reverse;
  }
  
  public boolean reversed()
  {
    return this.reverse;
  }
  
  
}
