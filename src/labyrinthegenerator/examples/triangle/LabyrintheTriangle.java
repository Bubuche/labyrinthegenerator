/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.triangle;

import labyrinthegenerator.model.impl.LabyrintheImpl;
import labyrinthegenerator.examples.l2d.Point2i;

/**
 *
 * @author Bubuche
 */
public class LabyrintheTriangle  extends LabyrintheImpl<Point2i, PieceTriangle>
{
  private int deepness;
  
  public LabyrintheTriangle(int deepness)
  {
    this.deepness = deepness;
  }
  
  public int deepness()
  {
    return this.deepness;
  }
}
