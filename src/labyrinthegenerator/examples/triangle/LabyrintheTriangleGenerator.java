/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.triangle;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import labyrinthegenerator.model.impl.AbstractLabyrintheRegulierGenerator;
import labyrinthegenerator.examples.l2d.Point2i;
import labyrinthegenerator.tools.Point2iGenerator;

/**
 *
 * @author Bubuche
 */
public class LabyrintheTriangleGenerator extends AbstractLabyrintheRegulierGenerator<Point2i, PieceTriangle, LabyrintheTriangle, Integer>
{
  private static Integer[] directions = new Integer[]
  {
    0,
    1,
    2
  };
  
  private PieceTriangle start, exit;
  
  private void addRecurse(int stepToDo, boolean reverse, Point2i ... sides)
  {
    if ( stepToDo == 0 )
    {
      int x, y;
      x = sides[1].getX();
      y = (sides[0].getY() + sides[1].getY()) / 2;
       
      Point2i id = Point2iGenerator.instance.get(x, y);
      PieceTriangle piece = new PieceTriangle(id, reverse);
      if ( start == null )
      {
        start = piece;
      }
      exit = piece;
      labyrinthe.addPiece(piece);
      return;
    }
    
    
    Point2i[] middles = new Point2i[3];
    
    middles[0] = midOf(sides[0], sides[1]);
    middles[1] = midOf(sides[1], sides[2]);
    middles[2] = midOf(sides[2], sides[0]);
    
    int n_step = stepToDo - 1;
    
    addRecurse(n_step, reverse, sides[0], middles[0], middles[2]);
    addRecurse(n_step, reverse, middles[0], sides[1], middles[1]);
    addRecurse(n_step, !reverse, middles[1], middles[2], middles[0]);
    addRecurse(n_step, reverse, middles[2], middles[1], sides[2]);
  }
  
  private Point2i midOf(Point2i p1, Point2i p2)
  {
    int x = (p1.getX() + p2.getX()) / 2;
    int y = (p1.getY() + p2.getY()) / 2;
    
    return Point2iGenerator.instance.get(x, y);
  }
  
  public void setSize(int deepness)
  {
    labyrinthe = new LabyrintheTriangle(deepness);
    start = exit = null;
    
    // with deepness+1, all the vertex are even, so all centers of triangles are int.
    int size;
    size = 1 << (deepness+1);
    
    Point2i[] sides = new Point2i[3];
    
    sides[0] = Point2iGenerator.instance.get(0, 0);
    sides[1] = Point2iGenerator.instance.get(size/2, size);
    sides[2] = Point2iGenerator.instance.get(size, 0);
    
    addRecurse(deepness, false, sides);
    
    
    labyrinthe.setStart(start);
    labyrinthe.setExit(exit);
    //Random random = new Random(getRandomSeed());
    /*m_point.x = 0;
    m_point.y = 0;
    labyrinthe.setStart(labyrinthe.get(m_point));
    
    
    m_point.x = width-1;
    m_point.y = height-1;
    labyrinthe.setExit(labyrinthe.get(m_point));*/
  }

  public LabyrintheTriangleGenerator(Random random)
  {
    super(random);
  }

  public LabyrintheTriangleGenerator(long randomSeed)
  {
    super(randomSeed);
  }

  @Override
  protected Integer[] getAllDirections()
  {
    return directions;
  }

  @Override
  protected Point2i cloneId(Point2i a)
  {
    return Point2iGenerator.instance.get(a.getX(), a.getY());
  }
  
  @Override
  protected Point2i selectPiece(List<Point2i> pieces, Random random)
  {
    return pieces.get(pieces.size() - 1);
  }

  @Override
  protected Point2i get(Point2i from, Integer direction)
  {
    PieceTriangle piece;
    piece = labyrinthe.get(from);
    
    int addx, addy;
    
    switch(direction)
    {
      case 0 : 
        addx = -1;
        addy =  0;
        break;
      case 1 :
        addx =  1;
        addy =  0;
        break;
      case 2 :
        addx =  0;
        addy =  -1;
        break;
      default:
        throw new Error("wrong direction: "+direction);
    }
    
    if ( piece.reversed() )
    {
      addx = -addx;
      addy = -addy;
    }
    
    Point2i p = Point2iGenerator.instance.get(from.getX() + addx, from.getY() + (addy*2));
    if ( labyrinthe.get(p) != null )
    {
      return p;
    }
    return null;
  }
}
