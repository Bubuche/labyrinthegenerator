/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.l3d;

import java.util.Random;
import labyrinthegenerator.model.impl.AbstractLabyrintheRegulierGenerator;

/**
 *
 * @author Bubuche
 */
public class Labyrinthe3DGenerator extends AbstractLabyrintheRegulierGenerator<Point3i, Piece3D, Labyrinthe3D, Point3i>
{
  private MutablePoint3i m_point = new MutablePoint3i();
  private static Point3i[] directions = new Point3i[]
  {
    new Point3i(1, 0, 0),
    new Point3i(0, 1, 0),
    new Point3i(-1, 0, 0),
    new Point3i(0, -1, 0),
    new Point3i(0, 0, 1),
    new Point3i(0, 0, -1)
  };

  public Labyrinthe3DGenerator(Random random)
  {
    super(random);
  }

  public Labyrinthe3DGenerator(long randomSeed)
  {
    super(randomSeed);
  }
  
  public static void main(String args[])
  {
    Labyrinthe3DGenerator a = new Labyrinthe3DGenerator(0);
    a.setSize(10, 10, 10);
    
    System.out.println(a.generate().getPieces().size());
  }
  
  public void setSize(int width, int height, int floors)
  {
    labyrinthe = new Labyrinthe3D(width, height, floors);
    for ( int i = 0; i < height; i++ )
    {
      for ( int j = 0; j < width; j++ )
      {
        for ( int k = 0; k < floors; k++ )
        {
          labyrinthe.addPiece(new Piece3D(new Point3i(j, i, k)));
        }
      }
    }
    Random random = new Random(getRandomSeed());
    m_point.x = 0;
    m_point.y = 0;
    labyrinthe.setStart(labyrinthe.get(m_point));
    
    
    m_point.x = width-1;
    m_point.y = height-1;
    labyrinthe.setExit(labyrinthe.get(m_point));
    
    
  }

  @Override
  protected Point3i get(Point3i from, Point3i direction)
  {
    int x = from.x + direction.x;
    int y = from.y + direction.y;
    int z = from.z + direction.z;
    
    if ( x < 0 || x >= labyrinthe.getWidth() 
      || y < 0 || y >= labyrinthe.getHeight() 
      || z < 0 || z >= labyrinthe.getFloors() 
       )
      return null;
    
    m_point.setX(x);
    m_point.setY(y);
    m_point.setZ(z);
    
    return m_point;
  }

  @Override
  protected Point3i[] getAllDirections()
  {
    return directions;
  }

  @Override
  protected Point3i cloneId(Point3i a)
  {
    return new Point3i(a.x, a.y, a.z);
  }
  
  private class MutablePoint3i extends Point3i
  {

    public MutablePoint3i()
    {
      super(0, 0, 0);
    }

    public void setX(int x)
    {
      this.x = x;
    }

    public void setY(int y)
    {
      this.y = y;
    }
    
    public void setZ(int z)
    {
      this.z = z;
    }
  }
}
