/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.l3d;

import labyrinthegenerator.model.impl.LabyrintheImpl;

/**
 *
 * @author Bubuche
 */
public class Labyrinthe3D extends LabyrintheImpl<Point3i, Piece3D>
{
  private int width;
  private int height;
  private int floors;

  public Labyrinthe3D(int width, int height, int floors)
  {
    this.width = width;
    this.height = height;
    this.floors = floors;
  }

  public int getWidth()
  {
    return width;
  }

  public int getHeight()
  {
    return height;
  }
  
  public int getFloors()
  {
    return floors;
  }
}
