/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.l3d;

/**
 *
 * @author Bubuche
 */
public class Point3i implements Comparable<Point3i>
{
  protected int x;
  protected int y;
  protected int z;
  
  public Point3i(int x, int y, int z)
  {
    this.x = x;
    this.y = y;
    this.z = z;
  }
  
  public int getX()
  {
    return this.x;
  }
  
  public int getY()
  {
    return this.y;
  }
  
  public int getZ()
  {
    return this.z;
  }

  @Override
  public int compareTo(Point3i o)
  {
    if ( o == this )
      return 0;
    
    if ( o == null )
      return 1;
    
    int v;
    v = Integer.compare(x, o.x);
    if ( v != 0 )
      return v;
    
    v = Integer.compare(y, o.y);
    if ( v != 0 )
      return v;
    
    v = Integer.compare(z, o.z);
    
    
    return v;
  }
}
