/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.circle;

/**
 *
 * @author Bubuche
 */
public class PieceCircleId implements Comparable<PieceCircleId>
{
  protected int level;
  protected int angle;

  public PieceCircleId(int level, int angle)
  {
    this.level = level;
    this.angle = angle;
  }

  public int getLevel()
  {
    return level;
  }

  public int getAngle()
  {
    return angle;
  }
  
  @Override
  public String toString()
  {
    return angle+" @ "+level;
  }

  @Override
  public int compareTo(PieceCircleId o)
  {
    if ( o == this )
    {
      return 0;
    }
    
    if ( o == null )
    {
      return 1;
    }
    
    int v;
    v = Integer.compare(level, o.level);
    if ( v != 0 )
    {
      return v;
    }
    
    v = Integer.compare(angle, o.angle);
    
    return v;
  }
}
