/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.circle;

import labyrinthegenerator.model.impl.LabyrintheImpl;

/**
 *
 * @author Bubuche
 */
public class LabyrintheCircle  extends LabyrintheImpl<PieceCircleId, PieceCircle>
{
  private int levels;
  
  public LabyrintheCircle(int levels)
  {
    this.levels = levels;
  }
  
  public int levels()
  {
    return this.levels;
  }
}

