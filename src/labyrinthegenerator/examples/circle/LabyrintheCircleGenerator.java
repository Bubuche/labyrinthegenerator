/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.circle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import labyrinthegenerator.examples.circle.LabyrintheCircleGenerator.Direction;
import labyrinthegenerator.model.impl.AbstractLabyrintheGenerator;
import labyrinthegenerator.examples.triangle.PieceTriangle;

/**
 *
 * @author Bubuche
 */
public class LabyrintheCircleGenerator extends AbstractLabyrintheGenerator<PieceCircleId, PieceCircle, LabyrintheCircle, PieceCircleId>
{
  public static enum Direction
  {
    Down,
    Left,
    Right,
    Up1,
    Up2;
  }
  
  private static Direction[] directions = Direction.values();
  
  private PieceTriangle start, exit;
  
  //this function is used to ensure that the same calculation is always done (avoid easily avoidables bugs related to approximations)
  private static float angleCreator(int top, int sub)
  {
    return (float) ((Math.PI * top) / sub);
  }
  
  public void setSize(int levels)
  {
    labyrinthe = new LabyrintheCircle(levels);
    start = exit = null;
    
    // with deepness+1, all the vertex are even, so all centers of triangles are int.
    int size;
    size = 1 << (levels+1);
    
    for ( int i = 0; i < levels; i++ )
    {
      for ( int j = 0; j <= i; j++ )
      {
        PieceCircle piece = new PieceCircle(new PieceCircleId(i, j));
        labyrinthe.addPiece(piece);
      }
    }
    
    
    
    //Random random = new Random(getRandomSeed());
    /*m_point.x = 0;
    m_point.y = 0;
    labyrinthe.setStart(labyrinthe.get(m_point));
    
    
    m_point.x = width-1;
    m_point.y = height-1;
    labyrinthe.setExit(labyrinthe.get(m_point));*/
  }

  public LabyrintheCircleGenerator(Random random)
  {
    super(random);
  }

  public LabyrintheCircleGenerator(long randomSeed)
  {
    super(randomSeed);
  }

  protected Direction[] getAllDirections()
  {
    return directions;
  }

  @Override
  protected PieceCircleId cloneId(PieceCircleId a)
  {
    return a;
  }
  
  @Override
  protected PieceCircleId selectPiece(List<PieceCircleId> pieces, Random random)
  {
    return pieces.get(pieces.size() - 1);
  }
  
  @Override
  protected List<PieceCircleId> getAllDirectionForPiece(PieceCircleId aPiece, Random random)
  {
    List<PieceCircleId> result = new ArrayList<>();
    
    float angle1;
    float angle2;
    
    float size = (float) ((Math.PI * 2) / (aPiece.level+1));
    angle1 = aPiece.angle * size;
    angle2 = (aPiece.angle+1) * size;
    
    if ( aPiece.getLevel() > 0 )
    {
      getBetweenAngles(aPiece.getLevel()-1, angle1, angle2, result);
    }
    
    if ( aPiece.getLevel() < labyrinthe.levels() - 1 )
    {
      getBetweenAngles(aPiece.getLevel()+1, angle1, angle2, result);
    }
    
    if ( aPiece.getLevel() > 0 )
    {
      int next = (aPiece.getAngle()+1) % (aPiece.getLevel()+1);
      result.add(new PieceCircleId(aPiece.getLevel(), next));
    }
    
    if ( aPiece.getLevel() > 1 )
    {
      int prec = aPiece.getAngle() - 1;
      if ( prec == -1 )
      {
        prec = aPiece.getLevel();
      }
      result.add(new PieceCircleId(aPiece.getLevel(), prec));
    }
    
    //angle1 = (float) (angle1 / Math.PI);
    //angle2 = (float) (angle2 / Math.PI);
    //System.out.println(aPiece+" in "+angle1+" and "+angle2+"="+result);
    
    Collections.shuffle(result, random);
    //System.out.println(aPiece+" -> "+result);
    return result;
  }
  
  private void getBetweenAngles(int level, float angle1, float angle2, List<PieceCircleId> result)
  {
    MutableCircleId m = new MutableCircleId();
    
    while ( angle2 < angle1 )
    {
      angle2 += Math.PI * 2;
    }
    
    int partNumber = level + 1;
    
    float p = (float) ((2*Math.PI) / (partNumber));
    
    int part = 0;
    while((part+1) * p < angle1)
    {
      part += 1;
    }
    
    m.setLevel(level);
    while(angleOf(part * p) < angle2 - 0.001)
    {
      m.setAngle(part);
      PieceCircle piece = labyrinthe.get(m);
      if ( piece == null )
      {
        throw new Error("error: "+m+" "+p+" "+angle2);
      }
      result.add(piece.getId());
      
      part += 1;
    }
  }
  
  private float angleOf(float angle)
  {
    while(angle >= 360)
    {
      angle -= 360;
    }
    return angle;
  }
  
  @Override
  protected PieceCircleId get(PieceCircleId from, PieceCircleId direction)
  {
    if ( labyrinthe.get(direction) == null )
    {
      return null;
    }
    return direction;
  }
  
  public static class MutableCircleId extends PieceCircleId
  {

    public MutableCircleId()
    {
      super(0, 0);
    }
    
    public void setAngle(int angle)
    {
      this.angle = angle;
    }
    
    public void setLevel(int level)
    {
      this.level = level;
    }
  }
}