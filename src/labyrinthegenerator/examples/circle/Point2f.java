/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.circle;

/**
 *
 * @author Bubuche
 */
public class Point2f implements Comparable<Point2f>
{
  protected float x;
  protected float y;
  
  public Point2f(float x, float y)
  {
    this.x = x;
    this.y = y;
  }
  
  public float x()
  {
    return this.x;
  }
  
  public float y()
  {
    return this.y;
  }
  
  @Override
  public String toString()
  {
    return "("+x+", "+y+")";
  }

  @Override
  public int compareTo(Point2f o)
  {
    if ( o == this )
    {
      return 0;
    }
    
    if ( o == null )
    {
      return 1;
    }
    
    int v;
    v = Float.compare(x, o.x);
    if ( v != 0 )
    {
      return v;
    }
    
    v = Float.compare(y, o.y);
    
    return v;
  }
}
