/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.circle;

import labyrinthegenerator.model.impl.PieceImpl;

/**
 *
 * @author Bubuche
 */
public class PieceCircle extends PieceImpl<PieceCircleId>
{
  public PieceCircle(PieceCircleId id)
  {
    super(id);
  }
  
  @Override
  public String toString()
  {
    return "id="+getId();
  }
}
