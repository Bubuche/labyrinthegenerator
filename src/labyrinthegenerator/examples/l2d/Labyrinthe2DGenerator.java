/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.l2d;

import java.util.List;
import java.util.Random;
import labyrinthegenerator.model.impl.AbstractLabyrintheRegulierGenerator;

/**
 *
 * @author Bubuche
 */
public class Labyrinthe2DGenerator extends AbstractLabyrintheRegulierGenerator<Point2i, Piece2D, Labyrinthe2D, Point2i>
{
  private MutablePoint2i m_point = new MutablePoint2i();
  private static Point2i[] directions = new Point2i[]
  {
    new Point2i(1, 0),
    new Point2i(0, 1),
    new Point2i(-1, 0),
    new Point2i(0, -1)
  };

  public Labyrinthe2DGenerator(Random random)
  {
    super(random);
  }

  public Labyrinthe2DGenerator(long randomSeed)
  {
    super(randomSeed);
  }
  
  public void setSize(int width, int height)
  {
    labyrinthe = new Labyrinthe2D(width, height);
    for ( int i = 0; i < height; i++ )
    {
      for ( int j = 0; j < width; j++ )
      {
        labyrinthe.addPiece(new Piece2D(new Point2i(j, i)));
      }
    }
    //Random random = new Random(getRandomSeed());
    m_point.x = 0;
    m_point.y = 0;
    labyrinthe.setStart(labyrinthe.get(m_point));
    
    
    m_point.x = width-1;
    m_point.y = height-1;
    labyrinthe.setExit(labyrinthe.get(m_point));
  }

  @Override
  protected Point2i get(Point2i from, Point2i direction)
  {
    int x = from.x + direction.x;
    int y = from.y + direction.y;
    
    if ( x < 0 || x >= labyrinthe.getWidth() || y < 0 || y >= labyrinthe.getHeight() )
      return null;
    
    m_point.setX(x);
    m_point.setY(y);
    
    return m_point;
  }

  @Override
  protected Point2i[] getAllDirections()
  {
    return directions;
  }

  @Override
  protected Point2i cloneId(Point2i a)
  {
    return new Point2i(a.x, a.y);
  }
  
  @Override
  protected Point2i selectPiece(List<Point2i> pieces, Random random)
  {
    return pieces.get(pieces.size() - 1);
  }
  
  private class MutablePoint2i extends Point2i
  {

    public MutablePoint2i()
    {
      super(0, 0);
    }

    public void setX(int x)
    {
      this.x = x;
    }

    public void setY(int y)
    {
      this.y = y;
    }
  }
}
