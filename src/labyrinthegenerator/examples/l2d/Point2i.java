/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.l2d;

/**
 *
 * @author Bubuche
 */
public class Point2i implements Comparable<Point2i>
{
  protected int x;
  protected int y;
  
  public Point2i(int x, int y)
  {
    this.x = x;
    this.y = y;
  }
  
  public int getX()
  {
    return this.x;
  }
  
  public int getY()
  {
    return this.y;
  }
  
  @Override
  public String toString()
  {
    return "("+x+", "+y+")";
  }

  @Override
  public int compareTo(Point2i o)
  {
    if ( o == this )
    {
      return 0;
    }
    
    if ( o == null )
    {
      return 1;
    }
    
    int v;
    v = Integer.compare(x, o.x);
    if ( v != 0 )
    {
      return v;
    }
    
    v = Integer.compare(y, o.y);
    
    return v;
  }
}
