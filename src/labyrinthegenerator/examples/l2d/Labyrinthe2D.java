/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package labyrinthegenerator.examples.l2d;

import labyrinthegenerator.model.impl.LabyrintheImpl;

/**
 *
 * @author Bubuche
 */
public class Labyrinthe2D extends LabyrintheImpl<Point2i, Piece2D>
{
  private int width;
  private int height;

  public Labyrinthe2D(int width, int height)
  {
    this.width = width;
    this.height = height;
  }

  public int getWidth()
  {
    return width;
  }

  public int getHeight()
  {
    return height;
  }
}
